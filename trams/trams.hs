data color = 
  Red |
  Yellow |
  Blue |
  Green |
  White

  deriving Show

-- via Wikipedia

toColor "1"  = (Red, Yellow)
toColor "2"  = (Blue, Red)
toColor "3"  = (Yellow, Blue)
toColor "6"  = (Blue, Blue)
toColor "7"  = (Green, Green)
toColor "8"  = (Green, Yellow)
toColor "9"  = (White, White)
toColor "10" = (Green, Blue)
toColor "16" = (White, Green)
toColor "18" = (White, Blue)
toColor "19" = (Blue, White)
toColor "20" = (Green, White)
toColor "21" = (Yellow, White)
toColor "23" = (Blue, Green)
toColor "24" = (Blue, Yellow)
toColor "25" = (Red, Green)
toColor "27" = (Red, Blue)
toColor "29" = (Blue, Blue)
toColor "30" = (White, Green)
toColor "36" = (Red, Red)
toColor "38" = (Blue, Yellow)
toColor "39" = (Red, Red)
toColor "40" = (Green, Green)
toColor "41" = (Green, Green)
toColor "43" = (White, White)
toColor "45" = (Red, Red)
toColor "47" = (Red, Red)
toColor "48" = (Blue, Blue)
toColor "49" = (Blue, Red)
toColor "51" = (Red, Yellow)
toColor "52" = (Green, Red)
toColor "54" = (Green, Red)
toColor "55" = (Red, Blue)
toColor "56" = (Yellow, White)
toColor "57" = (White, Blue)
toColor "58" = (White, Red)
toColor "59" = (Yellow, White)
toColor "60" = (White, Blue)
toColor "61" = (Yellow, Yellow)
toColor "62" = (Green, Green)
toColor "64" = (Red, Yellow)
toColor "65" = (Green, White)
toColor "100" = (Green, Yellow)
toColor "А"   = (Yellow, Yellow)