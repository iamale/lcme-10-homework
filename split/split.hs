import Data.Char

split ssym str =
  fst $ foldl (\(words, acc) sym ->
      if sym == ssym then (words ++ [acc], "")
                     else (words, acc ++ [sym])
    ) ([], "") (str ++ [ssym])

getWords str =
  filter (/= "") $ map (filter isAlpha) (split ' ' str)

avgLen str =
  average $ map length (getWords str)
  where
    average lst = (fromIntegral (sum lst)) / (fromIntegral (length lst))