module DropEvery where

dropEvery n lst = drop' n lst
  where
    drop' _ [] = []
    drop' 1 (_:ls) = drop' n ls
    drop' m (a:ls) = a : (drop' (m-1) ls)

leaveEvery n lst = leave' n lst
  where
    leave' _ [] = []
    leave' 1 (a:ls) = a : (leave' n ls)
    leave' m (_:ls) = leave' (m-1) ls