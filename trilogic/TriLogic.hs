-- Ale110 <ale@incrowd.ws> | bit.ly/mit-license

module TriLogic where

  data TriLogic =
    Yes |
    No |
    Unknown

    deriving (Eq, Read, Show)

  not3 Yes = No
  not3 No = Yes
  not3 Unknown = Unknown

  (&&&) Yes Yes = Yes
  (&&&) No _ = No
  (&&&) _ No = No
  (&&&) _ _ = Unknown

  (|||) No No = No
  (|||) Yes _ = Yes
  (|||) _ Yes = Yes
  (|||) _ _ = Unknown

  (==>) No _ = Yes
  (==>) Unknown _ = Yes
  (==>) Yes x = x