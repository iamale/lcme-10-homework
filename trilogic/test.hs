-- Ale110 <ale@incrowd.ws> | bit.ly/mit-license

import TriLogic

test1 = 
  [ ((x, y, Unknown), x ==> (y ==> x) == Yes) |
    x <- [Yes, No, Unknown],
    y <- [Yes, No, Unknown],
    z <- [Yes, No, Unknown]
  ]

test2 = 
  [ ((x, y, z), (x ==> y) ==> ((x ==> (y ==> z)) ==> (x ==> z)) == Yes) |
    x <- [Yes, No, Unknown],
    y <- [Yes, No, Unknown],
    z <- [Yes, No, Unknown]
  ]

test3 = [ ((x, Unknown, Unknown), not3 (not3 x) ==> x == Yes) | x <- [Yes, No, Unknown] ]

test4 =
  [ ((x, y, Unknown), x ==> ((not3 x) ==> y) == Yes) |
    x <- [Yes, No, Unknown],
    y <- [Yes, No, Unknown]
  ]

main = do
  print [
    ("test1", [fst fail | fail <- test1, not (snd fail)]),
    ("test2", [fst fail | fail <- test2, not (snd fail)]),
    ("test3", [fst fail | fail <- test3, not (snd fail)]),
    ("test4", [fst fail | fail <- test4, not (snd fail)])]