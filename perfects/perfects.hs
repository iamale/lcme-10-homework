perfects = [2^(n - 1) * (2^n - 1) | n <- primes, isPrime (2^n - 1)]
  where
    divisors n = [x | x <- [1..(n - 1)], rem n x == 0]
    isPrime n = (divisors n == [1])
    primes = [n | n <- [1..], isPrime n]