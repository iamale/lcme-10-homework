{- |
Module      :  ListsExt
Copyright   :  (c) Alexander Pushkov
License     :  MIT

Maintainer  :  ale@incrowd.ws
Stability   :  experimental
Portability :  portable
-}

module ListsExt where

  data ListExt a =
    Nil |
    Cons a (ListExt a)

  instance (Show a) => Show (ListExt a) where
    show lst =
      "[" ++ (take ((Prelude.length (show' lst))-2) (show' lst)) ++ "]"
      where
        show' Nil = ""
        show' (Cons a l) = (show a) ++ ", " ++ (show' l)

  sum Nil = 0
  sum (Cons a l) = a + (ListsExt.sum l)

  max_list Nil = -1/0
  max_list (Cons a l) = max a (max_list l)

  length Nil = 0
  length (Cons a l) = 1 + (ListsExt.length l)

  avg l = (ListsExt.sum l) / (ListsExt.length l)

  reverse Nil = Nil
  reverse (Cons a l) =
    (ListsExt.reverse l) `cat` (Cons a Nil)
    where
      cat Nil l2 = l2
      cat (Cons a1 l1) l2 = Cons a1 (cat l1 l2)