-- Currency data type
-- author: ale110 <ale@incrowd.ws>
-- WTFPL2

data Currency =
  RUR Float |
  USD Float |
  EUR Float |
  BYR Int
  deriving (Show, Read)

toByr (BYR n) = BYR n
toByr (RUR n) = BYR (truncate (270 * n))
-- Currency rates provided by Citibank N.A. via Google
toByr (USD n) = toByr (RUR (33.27 * n))
toByr (EUR n) = toByr (RUR (43.85 * n))

instance Eq Currency where
  (BYR m) == (BYR n) = m == n
  a == b = (toByr a) == (toByr b)

instance Ord Currency where
  compare (BYR m) (BYR n) = compare m n
  compare a b = compare (toByr a) (toByr b)

main = do
  print (USD 10 > BYR 10000)
  print (USD 1 > BYR 10000)