-- Currency data type
-- author: ale110 <ale@incrowd.ws>
-- WTFPL2

--- Окей, тут все легко.
--- Для начала определим тип данных:
data Currency =
  RUR Float |
  USD Float |
  EUR Float |
  BYR Int
  
  --- ...и зададим некоторые базовые свойства.
  --- Например, его можно читать и писать так же, как и другие типы в Haskell'е.
  deriving (Show, Read)

--- Чтобы продвинуться дальше, напишем функцию, которая бы все
--- переводила в белорусские рубли.
toByr (BYR n) = BYR n
toByr (RUR n) = BYR (truncate (270 * n))
-- Currency rates provided by Citibank N.A. via Google
toByr (USD n) = toByr (RUR (33.27 * n))
toByr (EUR n) = toByr (RUR (43.85 * n))

--- Сейчас научим Хаскель проверять валюты на равенство.
instance Eq Currency where
  --- Для этого белорусские рубли будем сравнивать как есть...
  (BYR m) == (BYR n) = m == n
  --- ...а остальные валюты переведем в белорусские рубли.
  a == b = (toByr a) == (toByr b)

--- То же самое повторим для операций `<`, `>`, `<=`, `>=`.
instance Ord Currency where
  compare (BYR m) (BYR n) = compare m n
  compare a b = compare (toByr a) (toByr b)

  --- Что такое `compare`, нам подскажет `info`:
  ---
  ---     Prelude> :info compare
  ---     class Eq a => Ord a where
  ---       compare :: a -> a -> Ordering
  ---       ...
  ---         -- Defined in `GHC.Classes'
  ---     Prelude> :info Ordering
  ---     data Ordering = LT | EQ | GT  -- Defined in `GHC.Types'
  ---     ...
  ---
  --- То есть `compare x y` вернет `LT`, если x меньше y,
  --- `EQ`, если x равно y и `GT`, если x больше y.

--- Остается только проверить, что мы все правильно сделали.
main = do
  print (USD 10 > BYR 10000) --- True
  print (USD 1 > BYR 10000) --- False