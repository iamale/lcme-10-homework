module DropEveryFib where

dropEveryFib lst = drop' 0 (0:1:(dropWhile (<2) fibs)) lst
  where
    fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

    drop' _ _ [] = []
    drop' i (f:fs) (e:ls) =
      if i == f
        then (drop' (i+1) fs ls)
        else e : (drop' (i+1) (f:fs) ls)