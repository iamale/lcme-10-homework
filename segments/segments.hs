{- |
Module      :  Segments
Copyright   :  (c) Alexander Pushkov, Ivan Bogdanov
License     :  MIT

Maintainer  :  ale@incrowd.ws
Stability   :  experimental
Portability :  portable
-}

module Segments where
  data Segment =
    None |
    Dot Int Int |
    Vert Int Int Int |
    Hori Int Int Int
    deriving (Show,Eq)


  canonize (None) = None
  canonize (Dot x y) = Dot x y

  canonize (Vert x y z) =
    if y == z then
      Dot x z
    else
      Vert x (min y z) (max y z)

  canonize (Hori x y z) =
    if x == y then
      Dot x z
    else
      Hori (min x y) (max x y) z


  intersect (Vert q w1 w2) (Vert x y1 y2) =
    if (q==x) then
      if (max w1 y1)>(min w2 y2) then
        None
      else if (max w1 y1)==(min w2 y2) then
        Dot x (max w1 y1)
      else
        Vert x (max w1 y1) (min w2 y2)
    else None

  intersect (Hori q1 q2 w) (Hori x1 x2 y) =
    if (w==y) then
      if (max q1 x1)>(min q2 x2) then
        None
      else if (max q1 x1)==(min q2 x2) then
        Dot (min q2 x2) y
      else
        Hori (max q1 x1) (min q2 x2) w
    else None

  intersect (Hori q1 q2 w) (Vert x y1 y2) =
    if (y1<w && w<y2) && (q1<x && x<q2) then Dot x w
    else None

  intersect (Vert q w1 w2) (Hori x1 x2 y) = intersect (Hori x1 x2 y) (Vert q w1 w2)

  intersect (Hori q1 q2 w) (Dot x y) =
    if (y==w && q1<=x) && (x<=q2) then Dot x y
    else None

  intersect (Dot x y) (Hori q1 q2 w) = intersect (Hori q1 q2 w) (Dot x y)

  intersect (Vert q w1 w2) (Dot x y) =
    if (x==q && w1<=y && y<=w2) then Dot x y
    else None

  intersect (Dot x y) (Vert q w1 w2) = intersect (Vert q w1 w2) (Dot x y)

  intersect (Dot x y) (Dot q w) =
    if (x==q && y==w) then
      Dot x y
    else None

  intersect _ _ = None