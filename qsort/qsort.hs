qsort [] = []

qsort (x:xs) = qsort [ y | y <- xs, y <= x ]
  ++ [x]
  ++ qsort [ y | y <- xs, y > x ]

main = do {
  print (qsort [0, 1, 8, 8, 4])
}